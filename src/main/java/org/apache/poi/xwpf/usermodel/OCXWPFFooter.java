package org.apache.poi.xwpf.usermodel;

import java.util.List;

public class OCXWPFFooter {

    private XWPFFooter footer;

    public OCXWPFFooter(XWPFFooter footer) {
        this.footer = footer;
    }

    /**
     * remove a BodyElement from bodyElements array list
     *
     * @param pos position
     * @return true if removing was successfully, else return false
     */
    public boolean removeBodyElement(int pos) {
        if (pos >= 0 && pos < footer.bodyElements.size()) {
            BodyElementType type = footer.bodyElements.get(pos).getElementType();
            if (type == BodyElementType.TABLE) {
                int tablePos = getTablePos(pos);
                footer.tables.remove(tablePos);
                footer._getHdrFtr().removeTbl(tablePos);
            }
            if (type == BodyElementType.PARAGRAPH) {
                int paraPos = getParagraphPos(pos);
                footer.paragraphs.remove(paraPos);
                footer._getHdrFtr().removeP(paraPos);
            }
            footer.bodyElements.remove(pos);

            return true;
        }
        return false;
    }

    /**
     * Finds that for example the 2nd entry in the body list is the 1st paragraph
     */
    private int getBodyElementSpecificPos(int pos, List<? extends IBodyElement> list) {
        // If there's nothing to find, skip it
        if (list.size() == 0) {
            return -1;
        }

        if (pos >= 0 && pos < footer.bodyElements.size()) {
            // Ensure the type is correct
            IBodyElement needle = footer.bodyElements.get(pos);
            if (needle.getElementType() != list.get(0).getElementType()) {
                // Wrong type
                return -1;
            }

            // Work back until we find it
            int startPos = Math.min(pos, list.size() - 1);
            for (int i = startPos; i >= 0; i--) {
                if (list.get(i) == needle) {
                    return i;
                }
            }
        }

        // Couldn't be found
        return -1;
    }

    /**
     * Look up the paragraph at the specified position in the body elements list
     * and return this paragraphs position in the paragraphs list
     *
     * @param pos The position of the relevant paragraph in the body elements
     *            list
     * @return the position of the paragraph in the paragraphs list, if there is
     * a paragraph at the position in the bodyelements list. Else it
     * will return -1
     */
    public int getParagraphPos(int pos) {
        return getBodyElementSpecificPos(pos, footer.paragraphs);
    }

    /**
     * get with the position of a table in the bodyelement array list
     * the position of this table in the table array list
     *
     * @param pos position of the table in the bodyelement array list
     * @return if there is a table at the position in the bodyelement array list,
     * else it will return null.
     */
    public int getTablePos(int pos) {
        return getBodyElementSpecificPos(pos, footer.tables);
    }
}
