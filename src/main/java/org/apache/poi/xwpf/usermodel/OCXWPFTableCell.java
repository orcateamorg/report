package org.apache.poi.xwpf.usermodel;

import java.util.List;

/**
 * XWPFTableCell hack
 * Using in DocXReport
 * - We need to remove any element according to position from bodyelement
 * - We need to add new paragraph end of cell
 * Methods are taken from XWPFDocument
 */
public class OCXWPFTableCell {

    private XWPFTableCell cell;

    public OCXWPFTableCell(XWPFTableCell cell) {
        this.cell = cell;
    }

    /**
     * remove a BodyElement from bodyElements array list
     *
     * @param pos position
     * @return true if removing was successfully, else return false
     */
    public boolean removeBodyElement(int pos) {
        if (pos >= 0 && pos < cell.bodyElements.size()) {
            BodyElementType type = cell.bodyElements.get(pos).getElementType();
            if (type == BodyElementType.TABLE) {
                int tablePos = getTablePos(pos);
                cell.tables.remove(tablePos);
                cell.getCTTc().removeTbl(tablePos);
            }
            if (type == BodyElementType.PARAGRAPH) {
                int paraPos = getParagraphPos(pos);
                cell.paragraphs.remove(paraPos);
                cell.getCTTc().removeP(paraPos);
            }
            cell.bodyElements.remove(pos);

            return true;
        }
        return false;
    }

    /**
     * Finds that for example the 2nd entry in the body list is the 1st paragraph
     */
    private int getBodyElementSpecificPos(int pos, List<? extends IBodyElement> list) {
        // If there's nothing to find, skip it
        if (list.size() == 0) {
            return -1;
        }

        if (pos >= 0 && pos < cell.bodyElements.size()) {
            // Ensure the type is correct
            IBodyElement needle = cell.bodyElements.get(pos);
            if (needle.getElementType() != list.get(0).getElementType()) {
                // Wrong type
                return -1;
            }

            // Work back until we find it
            int startPos = Math.min(pos, list.size() - 1);
            for (int i = startPos; i >= 0; i--) {
                if (list.get(i) == needle) {
                    return i;
                }
            }
        }

        // Couldn't be found
        return -1;
    }

    /**
     * Look up the paragraph at the specified position in the body elements list
     * and return this paragraphs position in the paragraphs list
     *
     * @param pos The position of the relevant paragraph in the body elements
     *            list
     * @return the position of the paragraph in the paragraphs list, if there is
     * a paragraph at the position in the bodyelements list. Else it
     * will return -1
     */
    public int getParagraphPos(int pos) {
        return getBodyElementSpecificPos(pos, cell.paragraphs);
    }

    /**
     * get with the position of a table in the bodyelement array list
     * the position of this table in the table array list
     *
     * @param pos position of the table in the bodyelement array list
     * @return if there is a table at the position in the bodyelement array list,
     * else it will return null.
     */
    public int getTablePos(int pos) {
        return getBodyElementSpecificPos(pos, cell.tables);
    }

    /**
     * Appends a new paragraph to this document
     *
     * @return a new paragraph
     */
    public XWPFParagraph createParagraph() {
        XWPFParagraph p = new XWPFParagraph(cell.getCTTc().addNewP(), cell);
        cell.bodyElements.add(p);
        cell.paragraphs.add(p);
        return p;
    }

    /**
     * Create an empty table with one row and one column as default.
     *
     * @return a new table
     */
    public XWPFTable createTable() {
        XWPFTable table = new XWPFTable(cell.getCTTc().addNewTbl(), cell);
        cell.bodyElements.add(table);
        cell.tables.add(table);
        return table;
    }

    /**
     * Create an empty table with a number of rows and cols specified
     *
     * @param rows rows
     * @param cols cols
     * @return table
     */
    public XWPFTable createTable(int rows, int cols) {
        XWPFTable table = new XWPFTable(cell.getCTTc().addNewTbl(), cell, rows, cols);
        cell.bodyElements.add(table);
        cell.tables.add(table);
        return table;
    }

}
