package org.orcateam.report;

import java.util.*;

/**
 * General OrcaReport Management Class.
 */
public abstract class OrcaReport {

    protected Object document;

    private Map<String, Object> context = new HashMap<String, Object>();

    /**
     * Report Renderer Method
     *
     * @throws Exception render exception
     * @return byte[] rendered report
     *
     */

    protected abstract byte[] init() throws Exception;

    private void initResourceBundle() {
        Map<String, String> messages = new HashMap<String, String>(0);
        try {
            String resource = (String) get(OrcaReportParam.RESOURCE_BUNDLE);
            Locale locale = (Locale) get(OrcaReportParam.LOCALE);
            locale = locale == null ? Locale.ENGLISH : locale;
            if (resource != null) {
                ResourceBundle bundle = ResourceBundle.getBundle(resource, locale);
                if (bundle != null) {
                    Enumeration<String> keys = bundle.getKeys();
                    while (keys.hasMoreElements()) {
                        String key = keys.nextElement();
                        messages.put(key, bundle.getString(key));
                    }
                }
            }
        } catch (Exception e) {
            //TODO necessary exception handling should be written.
        }
        put("m", messages);
    }

    public byte[] render() throws Exception {
        initResourceBundle();
        return init();
    }

    public void put(String key, Object value) {
        context.put(key, value);
    }

    public Object get(String key) {
        return context.get(key);
    }

    public Set<String> getContextKeySet() {
        return context.keySet();
    }

    public Map<String, Object> getContext() {
        return Collections.unmodifiableMap(context);
    }


    public Object getDocument() {
        return document;
    }

    public void setDocument(Object document) {
        this.document = document;
    }
}
