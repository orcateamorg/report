package org.orcateam.report.exception;

public class OrcaReportMetadataParserException extends Exception {

    public OrcaReportMetadataParserException() {
    }

    public OrcaReportMetadataParserException(String s) {
        super(s);
    }

    public OrcaReportMetadataParserException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public OrcaReportMetadataParserException(Throwable throwable) {
        super(throwable);
    }
}
