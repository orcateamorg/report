package org.orcateam.report.engine.xlsx;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.orcateam.report.OrcaReport;
import org.orcateam.report.exception.OrcaReportRendererException;
import org.orcateam.report.renderer.TableRenderer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExcelXTableRenderer extends TableRenderer {

    XSSFSheet sheet;

    public ExcelXTableRenderer(Object metaCell, Sheet sheet, OrcaReport orcaReport, String metaSring, Map<String, Object> context) throws Exception {
        super(metaCell, orcaReport, metaSring, context);
        this.sheet = (XSSFSheet) sheet;
        initMetadata();
    }

    @Override
    protected Integer getFirstColIndex() {
        Integer metaRowIndex = getMetaCell().getRowIndex();
        Row firstRow = sheet.getRow(metaRowIndex + 1);
        return firstRow.getCell(getMetaCell().getColumnIndex()).getColumnIndex();
    }

    @Override
    protected Integer getFirstRowIndex() {
        return getMetaCell().getRowIndex() + 1;
    }

    @Override
    public void render() throws OrcaReportRendererException {
        addDynamicColumns();
        addDynamicRows();
        getMetaCell().setCellValue(getMetadata("title").getValue());
    }

    public int renderAndGetTableRowSize() throws OrcaReportRendererException {
        render();
        return rowDataList.size();
    }


    private void addDynamicColumns() {
        if (columnData == null || columnData.getValue() == null || columnDataList == null) {
            dynamicColumnIndex = -1;
            return;
        }
    }

    private void addDynamicRows() throws OrcaReportRendererException {
        if (rowData == null || rowData.getValue() == null || rowDataList == null) {
            dynamicRowIndex = -1;
            return;
        }

        int regionSize = sheet.getNumMergedRegions();

        int startTableCol = -1, endTableCol = -1;

        for (int r = 0; r < regionSize; r++) {
            CellRangeAddress region = sheet.getMergedRegion(r);
            if (region.getFirstRow() == getMetaCell().getRowIndex()) {
                if (region.getFirstColumn() == getMetaCell().getColumnIndex()) {
                    startTableCol = region.getFirstColumn();
                    endTableCol = region.getLastColumn();
                    break;
                }
            }
        }

        if (startTableCol == -1 || endTableCol == -1) {
            // bizim exception.
        }

        int temprowIndex = (getFirstRowIndex() + dynamicRowIndex) - 1;
        Row temprow = sheet.getRow(temprowIndex);

        List<Integer> temrowRegionList = new ArrayList<Integer>();
        for (int r = 1; r < regionSize; r++) {
            CellRangeAddress region = sheet.getMergedRegion(r);
            if (region.getFirstRow() == temprowIndex && region.getLastRow() == temprowIndex) {
                temrowRegionList.add(r);
            }
        }

        int lastRowNum = sheet.getLastRowNum();
        int n = rowDataList.size();

        if (n != 0) {
            sheet.shiftRows(temprowIndex, lastRowNum, n);
            poiBugFix(temprowIndex + n, lastRowNum + n);
        }


        int dataRowIndex = 0, dataColIndex = 0;

        for (int i = 1; i <= rowDataList.size(); i++) {

            Row newrow = sheet.createRow(temprowIndex);

            copyRow(sheet.getWorkbook(), temprow, newrow);

            for (Integer r : temrowRegionList) {
                CellRangeAddress region = sheet.getMergedRegion(r);
                sheet.addMergedRegion(new CellRangeAddress(newrow.getRowNum(), newrow.getRowNum(), region.getFirstColumn(), region.getLastColumn()));
            }

            for (int colindex = startTableCol; colindex <= endTableCol; colindex++) {
                Cell newcell = newrow.getCell(colindex);
                if (newcell == null) {
                    continue;
                }
                Map<String, Object> context = getCellContext(dataRowIndex, dataColIndex);
                ExcelXCellRenderer cellRenderer = new ExcelXCellRenderer(newcell, getOrcaReport(), context);
                cellRenderer.render();
                dataColIndex++;
            }

            dataRowIndex++;
            temprowIndex++;

        }

        sheet.removeRow(temprow);

    }

    /**
     * Get context for cell
     *
     * @param rowindex row index
     * @param colindex col index
     * @return Map
     */
    public Map<String, Object> getCellContext(Integer rowindex, Integer colindex) {
        Map<String, Object> context = new HashMap<String, Object>();

        if (rowDataList == null && columnDataList == null) {
            return context;
        }

        if (columnDataList != null) {
            if (colindex >= 0 && colindex < columnDataList.size()) {
                context.put("colindex", colindex);
                Object columnVal = columnDataList.get(colindex);
                context.put(columnData.getVar(), columnVal);
            }
        }

        if (rowDataList != null) {
            if (rowindex >= 0 && rowindex < rowDataList.size()) {
                Object object = rowDataList.get(rowindex);
                context.put(rowData.getVar(), object);
                context.put("rowindex", rowindex);
            }
        }
        return context;
    }

    public Cell getMetaCell() {
        return (Cell) getBody();
    }


    private static Row copyRow(Workbook workbook, Row sourceRow, Row newRow) {

        for (int i = 0; i < sourceRow.getLastCellNum(); i++) {
            Cell sourceCell = sourceRow.getCell(i);

            if (sourceCell == null) {
                continue;
            }

            Cell newCell = newRow.createCell(i);

            CellStyle newCellStyle = workbook.createCellStyle();
            newCellStyle.cloneStyleFrom(sourceCell.getCellStyle());

            newCell.setCellStyle(newCellStyle);
            newCell.setCellType(sourceCell.getCellType());

            switch (sourceCell.getCellType()) {
                case BLANK:
                    newCell.setCellValue(sourceCell.getStringCellValue());
                    break;
                case STRING:
                    newCell.setCellValue(sourceCell.getRichStringCellValue());
                    break;
                case BOOLEAN:
                    newCell.setCellValue(sourceCell.getBooleanCellValue());
                    break;
                case NUMERIC:
                    newCell.setCellValue(sourceCell.getNumericCellValue());
                    break;
                case FORMULA:
                    newCell.setCellFormula(sourceCell.getCellFormula());
                    break;
            }
        }
        return newRow;

    }

    private void poiBugFix(int nFirstDstRow, int nLastDstRow) {
        for (int nRow = nFirstDstRow; nRow <= nLastDstRow; ++nRow) {
            final XSSFRow row = sheet.getRow(nRow);
            if (row != null) {
                String msg = "Row[rownum=" + row.getRowNum()
                        + "] contains cell(s) included in a multi-cell array formula. "
                        + "You cannot change part of an array.";
                for (Cell c : row) {
                    ((XSSFCell) c).updateCellReferencesForShifting(msg);
                }
            }
        }


    }

}
