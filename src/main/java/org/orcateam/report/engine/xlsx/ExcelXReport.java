package org.orcateam.report.engine.xlsx;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.orcateam.report.OrcaReport;
import org.orcateam.report.util.TokenUtil;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;

public class ExcelXReport extends OrcaReport {


    public ExcelXReport(InputStream inputStream) throws Exception {
        if (inputStream == null) {
            throw new Exception("Source file (Inputstream) is null.");
        }
        document = new XSSFWorkbook(inputStream);
    }

    @Override
    protected byte[] init() throws Exception {

        XSSFWorkbook workbook = (XSSFWorkbook) document;
        Iterator<Sheet> itsheet = workbook.iterator();

        while (itsheet.hasNext()) {
            Sheet sheet = itsheet.next();

            Integer lastrownum = sheet.getLastRowNum();
            Integer rownum = sheet.getFirstRowNum();
            while (rownum <= lastrownum) {
                Row row = sheet.getRow(rownum);
                if (row != null) {
                    Iterator<Cell> itcell = row.cellIterator();
                    while (itcell.hasNext()) {
                        Cell cell = itcell.next();

                        String cellval = cell.toString();
                        if (TokenUtil.isTableMetaStartToken(cellval)) {
                            ExcelXTableRenderer tableRenderer = new ExcelXTableRenderer(cell, sheet, this, cellval, new HashMap<String, Object>());
                            int tablerows = tableRenderer.renderAndGetTableRowSize() + 1;
                            rownum += tablerows;
                            lastrownum = sheet.getLastRowNum(); // the tableRender could have added new rows to sheet.
                        } else {
                            ExcelXCellRenderer cellRenderer = new ExcelXCellRenderer(cell, this, getContext());
                            cellRenderer.render();
                        }
                    }
                }
                rownum++;
            }

        }

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        ((XSSFWorkbook) document).write(output);
        return output.toByteArray();
    }


}
