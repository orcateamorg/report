package org.orcateam.report.engine.xlsx;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.orcateam.report.OrcaReport;
import org.orcateam.report.engine.docx.DocXEngine;
import org.orcateam.report.engine.docx.renderer.DocXRenderer;
import org.orcateam.report.exception.OrcaReportRendererException;
import org.orcateam.report.expression.ExpressionVal;

import java.util.Collection;
import java.util.Map;

public class ExcelXCellRenderer extends DocXRenderer {

    public ExcelXCellRenderer(Object body, OrcaReport orcaReport, Map<String, Object> context) {
        super(body, orcaReport, context, body.toString());

    }

    @Override
    public void render() throws OrcaReportRendererException {
        Collection<ExpressionVal> expressionValList = getExpressionValues();
        if (expressionValList != null && !expressionValList.isEmpty()) {
            for (ExpressionVal expressionVal : expressionValList) {
                boolean notFinished = true;
                try {
                    do {
                        String prevHash = DigestUtils.md5Hex(getCell().toString());
                        DocXEngine.replaceExcellCellText(expressionVal.getKey(), expressionVal.getValue(), getCell(), getOrcaReport());
                        String newHash = DigestUtils.md5Hex(getCell().toString());
                        String paragText = getCell().toString();
                        if (newHash.equals(prevHash)) {
                            throw new OrcaReportRendererException("Error while replacing : \"" + expressionVal.getKey() + "\". " +
                                    "Either you have given an expression that should be replaced with itself or something got fuxored. " +
                                    "If so, please report this issue to the Orcateam.");
                        } else if (!StringUtils.contains(paragText, expressionVal.getKey())) {
                            notFinished = false;
                        }
                    } while (notFinished);
                } catch (OrcaReportRendererException ex) {
                    // logger warning
                    ex.printStackTrace();
                }
            }
        }
    }

    public Cell getCell() {
        return (Cell) getBody();
    }
}
