package org.orcateam.report.engine.docx;

import org.orcateam.report.util.ImageUtil;

import java.io.IOException;
import java.io.InputStream;

public class ImageData {
    public String elName;
    public byte[] image;
    public int h;
    public int w;

    public void setImage(InputStream is) throws IOException {
        image = ImageUtil.convertInputStreamToByteArray(is);
    }
}
