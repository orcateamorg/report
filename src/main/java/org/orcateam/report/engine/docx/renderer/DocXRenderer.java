package org.orcateam.report.engine.docx.renderer;

import org.orcateam.report.OrcaReport;
import org.orcateam.report.OrcaReportRenderer;
import org.orcateam.report.expression.ExpressionVal;
import org.orcateam.report.util.TokenUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class DocXRenderer extends OrcaReportRenderer {

    protected List<String> tokenList = null;
    protected Map<String, Object> reportContext = new HashMap<String, Object>();
    private Object body;

    public DocXRenderer(Object body, OrcaReport orcaReport, Map<String, Object> context, String template) {
        super(orcaReport);
        this.body = body;
        for (String key : getOrcaReport().getContextKeySet()) {
            putContext(key, getOrcaReport().get(key));
        }
        if (context != null) {
            for (String key : context.keySet()) {
                putContext(key, context.get(key));
            }
        }
        initTokenList(template);
        initExpressionAndInvoke();
    }


    public void putContext(String key, Object value) {
        reportContext.put(key, value);
    }


    /**
     * Read token definition and init tokenList.
     */
    private void initTokenList(String template) {
        tokenList = TokenUtil.getTokenList(template);
    }

    /**
     * Invoke expression and putExpression to renderer context.
     */
    protected void initExpressionAndInvoke() {
        if (tokenList != null) {
            for (String token : tokenList) {
                //TODO burada el çevirilirken exceptionlar fırlatabiliyor. kullanıcıyı uyaracak exceptionlar alabiliriz.
                //Ambiguous statement, missing ';' between expressions gibi
                putExpression(token, new ExpressionVal(token, reportContext));
            }
        }
    }

    public Object getBody() {
        return body;
    }

}
