package org.orcateam.report.engine.docx.renderer;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.orcateam.report.OrcaReport;
import org.orcateam.report.engine.docx.DocXEngine;
import org.orcateam.report.exception.OrcaReportRendererException;
import org.orcateam.report.expression.ExpressionVal;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

/**
 * Orca Report Docx Paragraph Renderer Class.
 */
public class DocXParagraphRenderer extends DocXRenderer {


    public DocXParagraphRenderer(Object paragraph, OrcaReport orcaReport, Map<String, Object> context) {
        super(paragraph, orcaReport, context, ((XWPFParagraph) paragraph).getParagraphText());
    }


    /**
     * Render The Docx Paragraph
     * todo IOException, InvalidFormatException exception may change with orcaexception
     *
     * @throws OrcaReportRendererException render exception
     */
    @Override
    public void render() throws OrcaReportRendererException {
        Collection<ExpressionVal> expressionValList = getExpressionValues();
        if (expressionValList != null && !expressionValList.isEmpty()) {
            for (ExpressionVal expressionVal : expressionValList) {
                boolean notFinished = true;
                try {
                    do {
                        String prevHash = DigestUtils.md5Hex(getParagraph().getParagraphText());
                        DocXEngine.replaceParagraphText(expressionVal.getKey(), expressionVal.getValue(), getParagraph(), getOrcaReport());
                        String newHash = DigestUtils.md5Hex(getParagraph().getParagraphText());
                        String paragText = getParagraph().getParagraphText();
                        if (newHash.equals(prevHash)) {
                            throw new OrcaReportRendererException("Error while replacing : \"" + expressionVal.getKey() + "\". " +
                                    "Either you have given an expression that should be replaced with itself or something got fuxored. " +
                                    "If so, please report this issue to the Orcateam.");
                        } else if (!StringUtils.contains(paragText, expressionVal.getKey())) {
                            notFinished = false;
                        }
                    } while (notFinished);
                } catch (OrcaReportRendererException ex) {
                    // logger warning
                    ex.printStackTrace();
                } catch (InvalidFormatException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public XWPFParagraph getParagraph() {
        return (XWPFParagraph) getBody();
    }

}
