package org.orcateam.report.engine.docx;

import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.*;
import org.orcateam.report.OrcaReport;
import org.orcateam.report.engine.docx.renderer.DocXParagraphRenderer;
import org.orcateam.report.engine.docx.renderer.DocXTableRenderer;
import org.orcateam.report.exception.OrcaExpressionInvalidFormatException;
import org.orcateam.report.util.RenderedUtil;
import org.orcateam.report.util.TokenUtil;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

public class DocXReport extends OrcaReport {

    XWPFWordExtractor extractor = null;

    public DocXReport(InputStream inputStream) throws Exception {
        if (inputStream == null) {
            throw new Exception("Source file (Inputstream) is null.");
        }
        document = new XWPFDocument(inputStream);
        extractor = new XWPFWordExtractor((XWPFDocument) document);
    }

    protected byte[] init() throws Exception {
        renderReport();
        OutputStream output = new ByteArrayOutputStream();
        ((XWPFDocument) document).write(output);
        return ((ByteArrayOutputStream) output).toByteArray();
    }

    private void renderReport() throws Exception {
        renderHeaderFooter();
        renderBody((XWPFDocument) document, null);
    }

    /**
     * Renders header and footer's elements
     *
     * @throws Exception
     */
    private void renderHeaderFooter() throws Exception {
        for (XWPFHeader header : ((XWPFDocument) document).getHeaderList()) {
            renderBody(header, null);
        }
        for (XWPFFooter footer : ((XWPFDocument) document).getFooterList()) {
            renderBody(footer, null);
        }
    }

    /**
     * Iterates body elements for rendering
     * Removes if there is any element in list
     *
     * @param body    (XWPFTableCell or XWPFDocument)
     * @param context
     * @throws Exception
     */
    private void renderBody(IBody body, Map<String, Object> context) throws Exception {
        if (body == null) {
            return;
        }
        Iterator<IBodyElement> bodyIterator = body.getBodyElements().iterator();
        List<Integer> removingBodyElements = new ArrayList<Integer>(0);
        while (bodyIterator.hasNext()) {
            IBodyElement element = bodyIterator.next();
            renderElement(body, bodyIterator, removingBodyElements, element, context);
        }

        Collections.sort(removingBodyElements);

        // we need to remove meta tag paragraphs
        if (body instanceof XWPFTableCell) {
            XWPFTableCell tableCell = (XWPFTableCell) body;
            for (int i = removingBodyElements.size() - 1; i >= 0; i--) {
                new OCXWPFTableCell(tableCell).removeBodyElement(removingBodyElements.get(i));
            }
            // any cell needs a paragraph end of cell
            if (tableCell.getBodyElements().size() == 0
                    || tableCell.getParagraphs().size() == 0
                    || !(tableCell.getBodyElements().get(tableCell.getBodyElements().size() - 1) instanceof XWPFParagraph)) {
                new OCXWPFTableCell(tableCell).createParagraph();
            }
        } else if (body instanceof XWPFDocument) {
            for (int i = removingBodyElements.size() - 1; i >= 0; i--) {
                ((XWPFDocument) body).removeBodyElement(removingBodyElements.get(i));
            }
        } else if (body instanceof XWPFHeader) {
            XWPFHeader header = (XWPFHeader) body;
            for (int i = removingBodyElements.size() - 1; i >= 0; i--) {
                new OCXWPFHeader(header).removeBodyElement(removingBodyElements.get(i));
            }
        } else if (body instanceof XWPFFooter) {
            XWPFFooter footer = (XWPFFooter) body;
            for (int i = removingBodyElements.size() - 1; i >= 0; i--) {
                new OCXWPFFooter(footer).removeBodyElement(removingBodyElements.get(i));
            }
        }
    }

    /**
     * Renders given element of body
     *
     * @param body
     * @param bodyIterator
     * @param removingBodyElements
     * @param element
     * @param context
     * @throws Exception
     */
    private void renderElement(IBody body, Iterator<IBodyElement> bodyIterator, List<Integer> removingBodyElements, IBodyElement element, Map<String, Object> context) throws Exception {
        if (element == null) {
            return;
        }
        if (isElementInRemovingList(body, removingBodyElements, element)) {
            return;
        }
        switch (element.getElementType()) {
            case PARAGRAPH:
                // if the paragraph text matches with our table meta tag
                // we will render the next table
                if (isNextOrcaTable(element)) {
                    // TODO exceptions need to be specified
                    if (!bodyIterator.hasNext()) {
                        throw new OrcaExpressionInvalidFormatException();
                    }
                    IBodyElement tableElement = bodyIterator.next();
                    if (!bodyIterator.hasNext()) {
                        throw new OrcaExpressionInvalidFormatException();
                    }
                    IBodyElement tableEnd = bodyIterator.next();
                    if (!isOrcaTableEnd(tableEnd)) {
                        throw new OrcaExpressionInvalidFormatException();
                    }
                    String meta = ((XWPFParagraph) element).getParagraphText();
                    DocXTableRenderer tableRenderer = new DocXTableRenderer(tableElement, this, meta, context);
                    tableRenderer.render();
                    // we need to iterate each cell only if it has table
                    for (XWPFTableRow row : ((XWPFTable) tableElement).getRows()) {
                        for (XWPFTableCell cell : row.getTableCells()) {
                            int rowindex = ((XWPFTable) tableElement).getRows().indexOf(row);
                            int colindex = row.getTableCells().indexOf(cell);
                            Map<String, Object> cellContext = tableRenderer.getCellContext(rowindex, colindex);
                            renderBody(cell, cellContext);
                        }
                    }
                    // we are adding the position of table meta tag paragraph for remove
                    removingBodyElements.add(body.getBodyElements().indexOf(element));
                    // we are adding the position of table end meta tag paragraph for remove
                    removingBodyElements.add(body.getBodyElements().indexOf(tableEnd));
                } else if (isRenderedElement(element)) {
                    if (!bodyIterator.hasNext()) {
                        throw new OrcaExpressionInvalidFormatException();
                    }
                    removingBodyElements.add(body.getBodyElements().indexOf(element));
                    Boolean isRendered = RenderedUtil.checkRendered(((XWPFParagraph) element).getParagraphText(), this, context);
                    isRendered = isRendered == null ? false : isRendered;
                    Integer indexOfEL = body.getBodyElements().indexOf(element);
                    IBodyElement endElement = processRendered(body, bodyIterator, removingBodyElements, isRendered, indexOfEL);
                    if (isRendered) {
                        element = bodyIterator.next();
                    } else {
                        Integer index = body.getBodyElements().indexOf(endElement) + 1;
                        if (index >= body.getBodyElements().size()) {
                            element = null;
                        } else {
                            element = body.getBodyElements().get(index);
                        }
                    }
                    removingBodyElements.add(body.getBodyElements().indexOf(endElement));
                    renderElement(body, bodyIterator, removingBodyElements, element, context);
                } else {
                    DocXParagraphRenderer paragraphRenderer = new DocXParagraphRenderer(element, this, context);
                    paragraphRenderer.render();
                }
                break;
            case TABLE:
                for (XWPFTableRow row : ((XWPFTable) element).getRows()) {
                    for (XWPFTableCell cell : row.getTableCells()) {
                        renderBody(cell, context);
                    }
                }
                break;
        }
    }

    /**
     * Iterates body from given index and looks for rendered:end meta tag
     * - If this block will be rendered then does not move iterator's index,
     * if not moves iterator's index and
     * adds all of items between start and end tags to removing list
     * - If there is inner rendered condition handles it
     * - If there is no end meta tag then throws exception
     *
     * @param body
     * @param bodyIterator
     * @param removingBodyElements
     * @param isRendered
     * @param indexOfEL
     * @return IBodyElement
     * @throws Exception
     */
    private IBodyElement processRendered(IBody body, Iterator<IBodyElement> bodyIterator, List<Integer> removingBodyElements, Boolean isRendered, Integer indexOfEL) throws Exception {
        Integer handler = 0;
        for (int index = indexOfEL + 1; index < body.getBodyElements().size(); index++) {
            IBodyElement element = body.getBodyElements().get(index);
            if (element instanceof XWPFParagraph) {
                if (isRenderedElementEnd(element) && handler == 0) {
                    return element;
                }
                if (isRenderedElementEnd(element)) {
                    handler--;
                } else if (isRenderedElement(element)) {
                    handler++;
                }
            }

            if (!isRendered) {
                if (!bodyIterator.hasNext()) {
                    throw new OrcaExpressionInvalidFormatException();
                }
                removingBodyElements.add(body.getBodyElements().indexOf(element));
                bodyIterator.next();
            }
        }
        throw new OrcaExpressionInvalidFormatException();
    }

    /**
     * Checks if element is in removing elements list
     *
     * @param body
     * @param removingBodyElements
     * @param element
     * @return Boolean
     */
    private Boolean isElementInRemovingList(IBody body, List<Integer> removingBodyElements, IBodyElement element) {
        for (Integer index : removingBodyElements) {
            if (element.equals(body.getBodyElements().get(index))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks paragraph if has table meta tag
     *
     * @param element (XWPFParagraph)
     * @return Boolean
     */
    private Boolean isNextOrcaTable(Object element) {
        String template = ((XWPFParagraph) element).getParagraphText();
        return TokenUtil.isTableMetaStartToken(template);
    }

    /**
     * Checks paragraph if has table end meta tag
     *
     * @param element (XWPFParagraph)
     * @return Boolean
     */
    private Boolean isOrcaTableEnd(Object element) {
        String template = ((XWPFParagraph) element).getParagraphText();
        return TokenUtil.isTableMetaEndToken(template);
    }

    /**
     * Checks paragraph if has rendered meta tag
     *
     * @param element
     * @return Boolean
     */
    private Boolean isRenderedElement(Object element) {
        String template = ((XWPFParagraph) element).getParagraphText();
        return TokenUtil.isRenderedStartToken(template);
    }

    /**
     * Checks paragraph if has rendered end meta tag
     *
     * @param element
     * @return Boolean
     */
    private Boolean isRenderedElementEnd(Object element) {
        String template = ((XWPFParagraph) element).getParagraphText();
        return TokenUtil.isRenderedEndToken(template);
    }
}
