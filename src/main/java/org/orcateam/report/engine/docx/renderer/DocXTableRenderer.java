package org.orcateam.report.engine.docx.renderer;


import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTbl;
import org.orcateam.report.OrcaReport;
import org.orcateam.report.engine.docx.DocXEngine;
import org.orcateam.report.exception.OrcaReportRendererException;
import org.orcateam.report.renderer.TableRenderer;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

/**
 * OrcaReport Word Document Table Renderer
 * Meta Data
 * table    : dynamic row list, type List, default null
 * column   : dynamic column list, type List, default null
 * rowstart : dynamic column start column, type int, default last row
 * colstart : dynamic column start column, type int, default last column
 */
public class DocXTableRenderer extends TableRenderer {


    public DocXTableRenderer(Object table, OrcaReport orcaReport, String metaSring, Map<String, Object> context) throws Exception {
        super(table, orcaReport, metaSring, context);
        initMetadata();
    }

    @Override
    protected Integer getFirstColIndex() {
        return getTable().getRow(getTable().getNumberOfRows() - 1).getTableCells().size() - 1;
    }

    @Override
    protected Integer getFirstRowIndex() {
        return getTable().getNumberOfRows() - 1;
    }

    @Override
    public void render() throws OrcaReportRendererException {
        CTTbl ctTbl = getTable().getCTTbl();
        if (ctTbl.sizeOfTrArray() == 0) {
            return;
        }
        addDynamicColumns();
        addDynamicRows();
    }

    /**
     * Adds dynamic columns to each row of existing table
     *
     * @throws OrcaReportRendererException
     */
    private void addDynamicColumns() throws OrcaReportRendererException {

        CTTbl ctTbl = getTable().getCTTbl();

        // get table's meta data
        if (columnData == null || columnData.getValue() == null) {
            dynamicColumnIndex = -1;
            return;
        }

        if (columnDataList == null) {
            return;
        }
        // iterate rows
        for (int rowC = 0; rowC < ctTbl.sizeOfTrArray(); rowC++) {
            XWPFTableRow tabRow = getTable().getRow(rowC);
            CTRow ctRow = tabRow.getCtRow();

            // if it is specified in meta tag get reference column
            // otherwise it is the last column of row
            int tempCellC = tabRow.getTableCells().size() - 1;
            if (dynamicColumnIndex != -1) {
                tempCellC = dynamicColumnIndex;
            }

            // we need to backup static columns of cell

            // TODO what will we do if colstart index bigger than column index?
            if (tempCellC > tabRow.getCtRow().sizeOfTcArray()) {
                continue;
            }

            // create dynamic columns
            int dynamicColumnAddIndex = 0;
            try {
                dynamicColumnAddIndex = getDynamicColumnAddIndex(tabRow, dynamicColumnIndex);
            } catch (OrcaReportRendererException e) {
                e.printStackTrace();
            }

            //check if row needs to be iterated or stretched.
            if (tabRow.getCell(dynamicColumnAddIndex).getCTTc().getTcPr().isSetGridSpan()) {
                BigInteger gridSpanVal = tabRow.getCell(dynamicColumnAddIndex).getCTTc().getTcPr().getGridSpan().getVal();
                tabRow.getCell(dynamicColumnAddIndex).getCTTc().getTcPr().getGridSpan().setVal(gridSpanVal.add(BigInteger.valueOf(columnDataList.size() - 1)));
            } else {
                XWPFTableCell tempCell = tabRow.getCell(dynamicColumnAddIndex);
                //add new cells to the row.
                for (int colC = 0; colC < columnDataList.size() - 1; colC++) {
                    XWPFTableCell tableCell = new XWPFTableCell(ctRow.insertNewTc(dynamicColumnAddIndex + colC), tabRow, getTable().getBody());
                    tabRow.getTableCells().add(dynamicColumnAddIndex + colC, tableCell);
                    DocXEngine.copyCell(tempCell, tableCell);
                }
            }
        }
    }

    /**
     * Decides which cell represents the given colStart value.
     *
     * @param tabRow
     * @param colStartVal
     * @return int
     * @throws OrcaReportRendererException
     */
    private int getDynamicColumnAddIndex(XWPFTableRow tabRow, int colStartVal) throws OrcaReportRendererException {
        int colIndex = 0;
        for (int colC = 0; colC < tabRow.getTableCells().size(); colC++, colIndex++) {
            if (tabRow.getCell(colC).getCTTc().getTcPr().isSetGridSpan()) {
                colIndex = colIndex + (tabRow.getCell(colC).getCTTc().getTcPr().getGridSpan().getVal().intValue() - 1);
            }
            if (colIndex >= colStartVal) {
                return colC;
            }
        }
        throw new OrcaReportRendererException();
    }

    /**
     * Decides which cell represents the given col value.
     *
     * @param tabRow
     * @param col
     * @return int
     */
    private int getDynamicColumnDataIndex(XWPFTableRow tabRow, int col) {
        int colIndex = 0;
        for (int colC = 0; colC < col; colC++, colIndex++) {
            if (tabRow.getCell(colC).getCTTc().getTcPr().isSetGridSpan()) {
                colIndex = colIndex + (tabRow.getCell(colC).getCTTc().getTcPr().getGridSpan().getVal().intValue() - 1);
            }
        }
        return colIndex;
    }

    /**
     * Adds dynamic rows to existing table
     *
     * @throws OrcaReportRendererException
     */
    private void addDynamicRows() throws OrcaReportRendererException {
        CTTbl ctTbl = getTable().getCTTbl();

        if (rowData == null || rowData.getValue() == null) {
            dynamicRowIndex = -1;
            return;
        }

        // if it is specified in meta tag get reference row
        // otherwise it is the last row of table
        int tempRowC = ctTbl.sizeOfTrArray() - 1;
        if (dynamicRowIndex != null) {
            tempRowC = dynamicRowIndex;
        }

        // TODO what will we do if rowstart index bigger than row index?
        if (tempRowC > ctTbl.sizeOfTrArray()) {
            return;
        }

        // we need to backup static rows of table
        int dynamicRowAddC = tempRowC + 1;

        XWPFTableRow tempRow = getTable().getRow(tempRowC);

        // create dynamic columns
        for (int rowC = 0; rowC < (rowDataList.size() - 1); rowC++) {
            //XWPFTableRow newRow = new XWPFTableRow(tempRow.getCtRow(), getTable());
            XWPFTableRow newRow = getTable().insertNewTableRow(dynamicRowAddC);
            for (XWPFTableCell cell : tempRow.getTableCells()) {
                XWPFTableCell newCell = newRow.createCell();
                DocXEngine.copyCell(cell, newCell);
            }
        }
        dynamicRowIndex = tempRowC;
    }

    /**
     * Get context for cell
     *
     * @param rowindex row index
     * @param colindex col index
     * @return Map
     */
    public Map<String, Object> getCellContext(Integer rowindex, Integer colindex) {
        Map<String, Object> context = new HashMap<String, Object>();

        if (rowDataList == null && columnDataList == null) {
            return context;
        }

        if (columnDataList != null) {
            if (rowindex >= 0 && getTable().getRow(rowindex) != null) {
                colindex = getDynamicColumnDataIndex(getTable().getRow(rowindex), colindex);
                colindex = colindex - dynamicColumnIndex;
                if (colindex >= 0 && colindex < columnDataList.size()) {
                    context.put("colindex", colindex);
                    Object columnVal = columnDataList.get(colindex);
                    context.put(columnData.getVar(), columnVal);
                }
            }
        }

        if (rowDataList != null) {
            rowindex = rowindex - dynamicRowIndex;
            if (rowindex >= 0 && rowindex < rowDataList.size()) {
                Object object = rowDataList.get(rowindex);
                context.put(rowData.getVar(), object);
                context.put("rowindex", rowindex);
            }
        }
        return context;
    }

    public XWPFTable getTable() {
        return (XWPFTable) getBody();
    }

}
