package org.orcateam.report.engine.docx;


import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTR;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTText;
import org.orcateam.report.OrcaReport;
import org.orcateam.report.OrcaReportParam;
import org.orcateam.report.util.ELUtil;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

public class DocXEngine {
    /**
     * Copy table cell with all styles and content
     *
     * @param from_cell source cell
     * @param to_cell to cell
     */
    public static void copyCell(XWPFTableCell from_cell, XWPFTableCell to_cell) {
        copyCellStyle(from_cell, to_cell);

        Iterator<IBodyElement> bodyIterator = from_cell.getBodyElements().iterator();

        int paragraphPos = 0;
        //int currentPos = -1;
        while (bodyIterator.hasNext()) {
            IBodyElement element = bodyIterator.next();
            switch (element.getElementType()) {
                case PARAGRAPH:
                    XWPFParagraph newParagraph = null;
                    if (paragraphPos >= to_cell.getParagraphs().size()) {
                        /*
                        XmlCursor cursor = to_cell.getParagraphs().get(paragraphPos-1).getCTP().newCursor();
                        newParagraph = to_cell.insertNewParagraph(cursor);*/
                        newParagraph = new OCXWPFTableCell(to_cell).createParagraph();
                    } else {
                        newParagraph = to_cell.getParagraphs().get(paragraphPos);
                    }
                    copyParagraph((XWPFParagraph) element, newParagraph);
                    paragraphPos++;
                    break;

                case TABLE:
                    /*XmlCursor cursor = null;
                    int localCurrentPos = currentPos;
                    if (localCurrentPos < (to_cell.getBodyElements().size() -1)) {
                        localCurrentPos++;
                    }
                    IBodyElement lastElement = ((IBodyElement) to_cell.getBodyElements().get(localCurrentPos));
                    if (lastElement.getElementType().equals(BodyElementType.PARAGRAPH)) {
                        cursor = ((XWPFParagraph) lastElement).getCTP().newCursor();
                    } else if (lastElement.getElementType().equals(BodyElementType.TABLE)) {
                        cursor = ((XWPFTable) lastElement).getCTTbl().newCursor();
                    }
                    XWPFTable newTable = to_cell.insertNewTbl(cursor);*/
                    XWPFTable newTable = new OCXWPFTableCell(to_cell).createTable();
                    copyTableStyle((XWPFTable) element, newTable);
                    int rowIndex = 0;
                    for (XWPFTableRow row : ((XWPFTable) element).getRows()) {
                        XWPFTableRow newRow = newTable.getRow(rowIndex);
                        if (newRow == null) {
                            newRow = newTable.createRow();
                        }
                        int colIndex = 0;
                        for (XWPFTableCell cell : row.getTableCells()) {
                            XWPFTableCell newCell = newRow.getCell(colIndex);
                            if (newCell == null) {
                                newCell = newRow.createCell();
                            }
                            new OCXWPFTableCell(newCell).removeBodyElement(0);
                            copyCell(cell, newCell);
                            colIndex++;
                        }
                        rowIndex++;
                    }
                    break;
            }
            //currentPos++;
        }
    }

    /**
     * Copy table style
     *
     * @param from_table
     * @param to_table
     */
    private static void copyTableStyle(XWPFTable from_table, XWPFTable to_table) {
        try {
            to_table.getCTTbl().setTblPr(from_table.getCTTbl().getTblPr());
        } catch (Exception ignored) {
        }
    }

    /**
     * Copy table cell style
     *
     * @param from_cell
     * @param to_cell
     */
    private static void copyCellStyle(XWPFTableCell from_cell, XWPFTableCell to_cell) {
        try {
            to_cell.setColor(from_cell.getColor());
        } catch (Exception ignored) {
        }
        try {
            to_cell.getCTTc().setTcPr(from_cell.getCTTc().getTcPr());
        } catch (Exception ignored) {
        }
        try {
            if (from_cell.getVerticalAlignment() == null) {
                to_cell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.TOP);
            } else {
                to_cell.setVerticalAlignment(from_cell.getVerticalAlignment());
            }
        } catch (Exception ignored) {
        }
    }

    /**
     * Copy paragragh with runs
     *
     * @param from
     * @param to
     */
    private static void copyParagraph(XWPFParagraph from, XWPFParagraph to) {
        copyParagraphStyle(from, to);

        for (XWPFRun fromRun : from.getRuns()) {
            XWPFRun run = to.createRun();
            run.getCTR().set(fromRun.getCTR().copy());
        }
    }

    /**
     * Copy paragraph style
     *
     * @param from
     * @param to
     */
    private static void copyParagraphStyle(XWPFParagraph from, XWPFParagraph to) {
        try {
            to.setStyle(from.getStyle());
        } catch (Exception ignored) {
        }
        try {
            to.setAlignment(from.getAlignment());
        } catch (Exception ignored) {
        }
        try {
            to.setVerticalAlignment(from.getVerticalAlignment());
        } catch (Exception ignored) {
        }
        try {
            to.setIndentationFirstLine(from.getIndentationFirstLine());
        } catch (Exception ignored) {
        }
    }


    public static void replaceExcellCellText(String replaceKey, Object replacingObject, Cell cell, OrcaReport orcaReport) {
        // Setting null string
        String nullString = (String) orcaReport.get(OrcaReportParam.NULL_STRING);
        nullString = nullString == null ? "null" : nullString;

        // Setting replace value
        String replaceValue = replacingObject == null ? nullString : replacingObject.toString();

        cell.setCellValue(replaceValue);
    }

    /**
     * Remove Paragraph content and set new template text
     *
     * @param replaceKey key
     * @param replacingObject object
     * @param paragraph paragraph
     * @param orcaReport orca context
     * @throws IOException io-exception
     * @throws InvalidFormatException invalid format
     */
    public static void replaceParagraphText(String replaceKey, Object replacingObject, XWPFParagraph paragraph, OrcaReport orcaReport) throws IOException, InvalidFormatException {
        // Setting null string
        String nullString = (String) orcaReport.get(OrcaReportParam.NULL_STRING);
        nullString = nullString == null ? "null" : nullString;

        String replaceValue;
        if (replacingObject instanceof ImageData) {
            replaceValue = ((ImageData) replacingObject).elName;
        } else {
            replaceValue = replacingObject == null ? nullString : replacingObject.toString();
        }

        List<XWPFRun> runList = paragraph.getRuns();

        ParagraphDivision indexes = findParagraphDivIndexes(runList, replaceKey, replaceValue);
        if (StringUtils.equalsIgnoreCase(replaceKey, indexes.getFoundText())) {
            if (indexes.getStartRunIndex() != null && indexes.getEndRunIndex() != null) {

                XWPFRun startRun = runList.get(indexes.getStartRunIndex());


                int startRunCTTextSize = startRun.getCTR().getTList().size();
                int startVarIndex = getCTTextIndexFromRun(startRun, "$");

                StringBuilder keepStartText = new StringBuilder();
                String keepStartTextTemp = startRun.getCTR().getTList().get(startVarIndex).getStringValue();

                for (int i = 0; i < keepStartTextTemp.indexOf('$'); i++) {
                    keepStartText.append(keepStartTextTemp.charAt(i));
                }

                for (int r = startRunCTTextSize - 1; r >= startVarIndex; r--) {
                    startRun.getCTR().removeT(r);
                }

                int removeIndexBegin = indexes.getStartRunIndex();
                int removeIndexEnd = indexes.getEndRunIndex();

                if (replacingObject instanceof ImageData) {
                    startRun.setText(keepStartText.toString());
                    int pictureType = XWPFDocument.PICTURE_TYPE_PNG;
                    String filename = ((ImageData) replacingObject).elName;
                    int width = Units.toEMU(((ImageData) replacingObject).w);
                    int height = Units.toEMU(((ImageData) replacingObject).h);
                    InputStream image = new ByteArrayInputStream(((ImageData) replacingObject).image);
                    startRun.addPicture(image, pictureType, filename, width, height);
                    removeIndexBegin++;
                } else {
                    String newValue = keepStartText + replaceValue;
                    if (StringUtils.isNotBlank(newValue)) {
                        startRun.setText(newValue);
                        removeIndexBegin++;
                    }
                }

                if (!indexes.getStartRunIndex().equals(indexes.getEndRunIndex())) {
                    XWPFRun endRun = runList.get(indexes.getEndRunIndex());
                    int endVarIndex = getCTTextIndexFromRun(endRun, "}");
                    int endRunCTTextSize = endRun.getCTR().getTList().size();
                    String keepEndText = "";
                    String keepEndTextTemp = endRun.getCTR().getTList().get(endVarIndex).getStringValue();
                    for (int i = keepEndTextTemp.length() - 1; i > keepEndTextTemp.indexOf('}'); i--) {
                        keepEndText = keepEndTextTemp.charAt(i) + keepEndText;
                    }
                    for (int r = endRunCTTextSize - endVarIndex - 1; r >= 0; r--) {
                        endRun.getCTR().removeT(r);
                    }
                    if (StringUtils.isNotBlank(keepEndText)) {
                        endRun.setText(keepEndText);
                        removeIndexEnd--;
                    }
                }

                for (int removeRunIndex = removeIndexEnd; removeRunIndex >= removeIndexBegin; removeRunIndex--) {
                    paragraph.removeRun(removeRunIndex);
                }
            }
        }
    }

    private static Integer getCTTextIndexFromRun(XWPFRun run, String findText) {
        Integer index = null;
        for (int i = 0; i < run.getCTR().getTList().size(); i++) {
            CTText text = run.getCTR().getTList().get(i);
            if (StringUtils.containsIgnoreCase(text.getStringValue(), findText)) {
                index = i;
                break;
            }
        }
        return index;
    }

    private static ParagraphDivision findParagraphDivIndexes(List<XWPFRun> runList, String replaceKey, String replaceValue) {
        Integer startRunIndex = null;
        Integer endRunIndex = null;

        Integer startVarIndex = null;
        Integer endVarIndex = null;
        String foundText = "";

        // find start and end runs
        for (int runIndex = 0; runIndex < runList.size(); runIndex++) {

            XWPFRun run = runList.get(runIndex);
            String runText = run.getText(run.getTextPosition());

            // cause image render fail
            // replace image el with text
            /*String newText = StringUtils.replace(runText, replaceKey, replaceValue);

            if (newText != null && !newText.equals(runText)) {
                replaceRunText(run, newText);
                runText = newText;
            }*/

            if (runText == null) {
                continue;
            }
            boolean isReplaceTextFound = false;
            for (int charIndex = 0; charIndex < runText.length(); charIndex++) {
                Character sch = runText.charAt(charIndex);
                if (startRunIndex == null) {
                    if (sch.equals('$')) {
                        Character ech = null;
                        if (charIndex + 1 < runText.length()) {
                            ech = runText.charAt(charIndex + 1);
                        } else {
                            if (runIndex + 1 < runList.size()) {
                                XWPFRun nextRun = runList.get(runIndex + 1);
                                String nextText = nextRun.getText(nextRun.getTextPosition());
                                if (nextText.length() > 0) {
                                    ech = nextText.charAt(0);
                                }
                            }
                        }
                        if (ech != null && ech.equals('{')) {
                            foundText = sch.toString();
                            startRunIndex = runIndex;
                            startVarIndex = charIndex;
                        }
                    }
                } else {
                    foundText = foundText + sch;
                    if (sch.equals('}')) {
                        endRunIndex = runIndex;
                        endVarIndex = charIndex;
                        foundText = ELUtil.apostropheCorrection(foundText);
                        if (replaceKey.equals(foundText)) {
                            isReplaceTextFound = true;
                            break;
                        } else {
                            startRunIndex = null;
                            endRunIndex = null;
                            startVarIndex = null;
                            endVarIndex = null;
                            foundText = "";
                        }
                    }
                }
            }

            if (isReplaceTextFound) {
                break;
            }
        }

        return new ParagraphDivision(startRunIndex, endRunIndex, startVarIndex, endVarIndex, foundText);
    }

    /**
     * Remove all test sequence in target run and set new text.
     *
     * @param run run
     * @param text text
     */
    public static void replaceRunText(XWPFRun run, String text) {
        CTR ctr = run.getCTR();
        for (int r = 0; r < ctr.sizeOfTArray(); r++) {
            ctr.removeT(r);
        }
        run.setText(text);
        run.getTextPosition();
    }

    private static class ParagraphDivision {
        Integer startRunIndex = null;
        Integer endRunIndex = null;

        Integer startVarIndex = null;
        Integer endVarIndex = null;
        String foundText = "";

        private ParagraphDivision(Integer startRunIndex, Integer endRunIndex, Integer startVarIndex, Integer endVarIndex, String foundText) {
            this.startRunIndex = startRunIndex;
            this.endRunIndex = endRunIndex;
            this.startVarIndex = startVarIndex;
            this.endVarIndex = endVarIndex;
            this.foundText = foundText;
        }

        public Integer getStartRunIndex() {
            return startRunIndex;
        }

        public Integer getEndRunIndex() {
            return endRunIndex;
        }

        public Integer getStartVarIndex() {
            return startVarIndex;
        }

        public Integer getEndVarIndex() {
            return endVarIndex;
        }

        public String getFoundText() {
            return foundText;
        }
    }

}
