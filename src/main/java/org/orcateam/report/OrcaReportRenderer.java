package org.orcateam.report;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.orcateam.report.exception.OrcaReportRendererException;
import org.orcateam.report.expression.ExpressionVal;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public abstract class OrcaReportRenderer {

    private OrcaReport orcaReport;

    private Map<String, OrcaReportMetadata> metaDataMap = new HashMap<String, OrcaReportMetadata>();

    private Map<String, ExpressionVal> expValMap = new HashMap<String, ExpressionVal>();

    public abstract void render() throws OrcaReportRendererException, IOException, InvalidFormatException;

    public OrcaReportRenderer(OrcaReport orcaReport) {
        this.orcaReport = orcaReport;
    }

    public void putExpression(String expression, ExpressionVal expressionVal) {
        expValMap.put(expression, expressionVal);
    }

    public ExpressionVal getExpression(String expression) {
        return expValMap.get(expression);
    }

    public Collection<ExpressionVal> getExpressionValues() {
        return expValMap.values();
    }

    public Set<String> getExpressionKeySet() {
        return expValMap.keySet();
    }

    public void putMetadata(String key, OrcaReportMetadata metaData) {
        metaDataMap.put(key, metaData);
    }

    public OrcaReportMetadata getMetadata(String key) {
        return metaDataMap.get(key);
    }

    public OrcaReport getOrcaReport() {
        return orcaReport;
    }

}
