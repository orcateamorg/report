package org.orcateam.report.util;

import org.orcateam.report.OrcaReport;
import org.orcateam.report.OrcaReportMetadata;
import org.orcateam.report.exception.OrcaExpressionInvalidFormatException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RenderedUtil {
    /**
     * Gets rendered el text, parses it and checks el if true or not
     *
     * @param metaText       has to be string to use for all engines
     * @param orcaReport orca report context
     * @param elementContext element context
     * @return Boolean
     * @throws Exception render exception
     */
    public static Boolean checkRendered(String metaText, OrcaReport orcaReport, Map<String, Object> elementContext) throws Exception {
        List<OrcaReportMetadata> metadataList = MetadataUtil.parse(metaText);
        if (metadataList.size() != 1) {
            throw new OrcaExpressionInvalidFormatException();
        }
        OrcaReportMetadata metadata = metadataList.get(0);
        return (Boolean) ELUtil.invoke(metadata.getValue(), mergeContext(orcaReport, elementContext));
    }

    /**
     * Merges orca base context and element context
     *
     * @param orcaReport
     * @param elementContext
     * @return Map
     */
    private static Map<String, Object> mergeContext(OrcaReport orcaReport, Map<String, Object> elementContext) {
        Map<String, Object> context = new HashMap<String, Object>(0);
        if (orcaReport.getContext() != null) {
            for (Map.Entry<String, Object> entry : orcaReport.getContext().entrySet()) {
                context.put(entry.getKey(), entry.getValue());
            }
        }
        if (elementContext != null) {
            for (Map.Entry<String, Object> entry : elementContext.entrySet()) {
                context.put(entry.getKey(), entry.getValue());
            }
        }
        return context;
    }
}
