package org.orcateam.report.util;

import org.apache.commons.jexl2.Expression;
import org.apache.commons.jexl2.JexlContext;
import org.apache.commons.jexl2.JexlEngine;
import org.apache.commons.jexl2.MapContext;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

public class ELUtil {
    public static Object invoke(String expression, Map<String, Object> context) {
        JexlEngine jexl = new JexlEngine();
        Expression el = jexl.createExpression(expression);
        JexlContext elContext = new MapContext();
        for (String property : context.keySet()) {
            elContext.set(property, context.get(property));
        }
        return el.evaluate(elContext);
    }

    /**
     * If EL used inside code, the apostrophes around the ‘expression’ should be replaced with
     * the apostrophes like 'expression' to make it work.
     *
     * @param expression el defination
     * @return String
     */
    public static String apostropheCorrection(String expression) {
        if (StringUtils.contains(expression, "‘")) {
            expression = StringUtils.replace(expression, "‘", "'");
        }
        if (StringUtils.contains(expression, "’")) {
            expression = StringUtils.replace(expression, "’", "'");
        }
        return expression;
    }
}
