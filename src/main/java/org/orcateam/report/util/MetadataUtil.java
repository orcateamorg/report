package org.orcateam.report.util;


import org.apache.commons.lang3.StringUtils;
import org.orcateam.report.OrcaReportMetadata;

import java.util.ArrayList;
import java.util.List;

public class MetadataUtil {

    /**
     * Parse OrcaReportComponent OrcaReportMetadata definition.
     * Example: ${table:peopleList as people; column:yearList as year}
     * new OrcaReportMetadata({key:table, var:people, value:peopleList})
     * new OrcaReportMetadata({key:column, var:year, value:yearList})
     *
     * @param metadataString el meta defination
     * @return meta list
     */
    public static List<OrcaReportMetadata> parse(String metadataString) {
        List<OrcaReportMetadata> metaDataList = new ArrayList<OrcaReportMetadata>();
        if (!StringUtils.isEmpty(metadataString)) {
            metadataString = StringUtils.remove(StringUtils.remove(metadataString, "${"), "}");
            metadataString = StringUtils.trim(metadataString);
            String[] definitions = StringUtils.split(metadataString, ";");
            for (String def : definitions) {
                def = StringUtils.trim(def);
                String[] components = StringUtils.split(def, ":");
                OrcaReportMetadata metaData = new OrcaReportMetadata();
                metaData.setKey(StringUtils.trim(components[0]));
                if (components.length > 1) {
                    String valDef = StringUtils.trim(components[1]);
                    metaData.setValue(valDef);
                    if (StringUtils.contains(valDef, " as ")) {
                        valDef = StringUtils.replace(valDef, " as ", " => ");
                        String[] split = StringUtils.split(valDef, "=>");
                        metaData.setVar(StringUtils.deleteWhitespace(split[1]));
                        metaData.setValue(StringUtils.deleteWhitespace(split[0]));
                    }
                }
                metaDataList.add(metaData);
            }
        }
        return metaDataList;
    }

}
