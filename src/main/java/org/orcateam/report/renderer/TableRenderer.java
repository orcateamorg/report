package org.orcateam.report.renderer;

import org.orcateam.report.OrcaReport;
import org.orcateam.report.OrcaReportMetadata;
import org.orcateam.report.engine.docx.renderer.DocXRenderer;
import org.orcateam.report.util.ELUtil;
import org.orcateam.report.util.MetadataUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * OrcaReport Word Document Table Renderer
 * Meta Data
 * table    : dynamic row list, type List, default null
 * column   : dynamic column list, type List, default null
 * rowstart : dynamic column start column, type int, default last row
 * colstart : dynamic column start column, type int, default last column
 */
public abstract class TableRenderer extends DocXRenderer {

    protected Integer dynamicColumnIndex = null;
    protected Integer dynamicRowIndex = null;
    protected String metaSring = null;

    protected OrcaReportMetadata rowData = null;
    protected OrcaReportMetadata columnData = null;

    protected List<Object> rowDataList = null;
    protected List<Object> columnDataList = null;
    protected Map<String, Object> tableContext = new HashMap<String, Object>();

    public TableRenderer(Object table, OrcaReport orcaReport, String metaSring, Map<String, Object> context) throws Exception {
        super(table, orcaReport, context, null);
        if (context != null) {
            tableContext.putAll(context);
            tableContext.putAll(orcaReport.getContext());
        } else {
            tableContext.putAll(orcaReport.getContext());
        }
        this.metaSring = metaSring;
    }

    /**
     * Read OrcaDocXTable metadata definition
     * @throws Exception render exception
     */
    protected void initMetadata() throws Exception {
        String metaDataString = metaSring;

        //init metadata context
        List<OrcaReportMetadata> metaDataList = MetadataUtil.parse(metaDataString);
        for (OrcaReportMetadata metaData : metaDataList) {
            putMetadata(metaData.getKey(), metaData);
        }

        rowData = getMetadata("table");
        columnData = getMetadata("column");
        OrcaReportMetadata colStart = getMetadata("colstart");
        OrcaReportMetadata rowStart = getMetadata("rowstart");
        OrcaReportMetadata title = getMetadata("title");

        //init data source
        if (rowData != null && rowData.getValue() != null) {
            rowDataList = (List<Object>) ELUtil.invoke(rowData.getValue(), tableContext);
        }

        //init column source
        if (columnData != null && columnData.getValue() != null) {
            columnDataList = (List<Object>) ELUtil.invoke(columnData.getValue(), tableContext);
        }

        //init colStart value
        if (colStart != null) {
            dynamicColumnIndex = Integer.parseInt(colStart.getValue());
        } else {
            dynamicColumnIndex = getFirstColIndex();
        }

        //init rowStart value
        if (rowStart != null) {
            dynamicRowIndex = Integer.parseInt(rowStart.getValue());
        } else {
            dynamicRowIndex = getFirstRowIndex();
        }

    }

    protected abstract Integer getFirstColIndex();

    protected abstract Integer getFirstRowIndex();


}
