package org.orcateam.report;

import org.junit.Test;
import org.orcateam.report.engine.docx.DocXReport;
import org.orcateam.report.engine.docx.ImageData;

import java.io.InputStream;

public class ImageTest extends BaseOrcaTest {
    @Test
    public void test() throws Exception {
        InputStream inputStream = getClass().getResourceAsStream("/ImageTest.docx");
        OrcaReport orcaReport = new DocXReport(inputStream);

        orcaReport.put(OrcaReportParam.NULL_STRING, "...");
        orcaReport.put("nullExpression", null);
        orcaReport.put("notNullExpression", "not null");
        orcaReport.put("renderedCell", null);
        orcaReport.put("cins", "cinsText");
        orcaReport.put("test", "testText");

        ImageData imageData = new ImageData();
        imageData.elName = "resima";
        imageData.setImage(getClass().getResourceAsStream("/testPattern.png"));
        imageData.h = 80;
        imageData.w = 128;

        orcaReport.put(imageData.elName, imageData);

        try {
            byte[] output = orcaReport.render();
            writeTestResults(output, ".docx");
            //String results = writeTestResults(output);
            //c(results);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
