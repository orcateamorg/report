package org.orcateam.report;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;

import java.io.*;

public abstract class BaseOrcaTest {

    private String testPath = null;

    @Before
    public void init() {

        String relPath = getClass().getProtectionDomain().getCodeSource().getLocation().getFile();
        File targetDir = new File(relPath + "../../target/rendered-reports/");
        if (!targetDir.exists()) {
            targetDir.mkdir();
        }

        testPath = targetDir.getPath();

        Assert.assertFalse(StringUtils.isBlank(testPath));
    }

    public String writeTestResults(byte[] output, String ext) {
        String fileName = getTestPath() + DigestUtils.md5Hex(Long.toString(System.currentTimeMillis())) + ext;
        OutputStream outputStream;
        try {
            outputStream = new FileOutputStream(new File(fileName));
            outputStream.write(output);
            outputStream.flush();
            outputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        return fileName;
    }


    public String getTestPath() {
        return testPath;
    }

}
