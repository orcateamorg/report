package org.orcateam.report;

import org.junit.Test;
import org.orcateam.report.engine.docx.DocXReport;

import java.io.InputStream;
import java.util.*;

public class TableInsideTableFormTest extends BaseOrcaTest {

    @Test
    public void test() throws Exception {

        InputStream inputStream = getClass().getResourceAsStream("/TableInsideTableForm.docx");
        OrcaReport orcaReport = new DocXReport(inputStream);

        // table:source
        List<People> peopleList = generatePeopleList(4, 20);
        List<People> differentPeopleList = generatePeopleList(2, 50);


        //column:source
        List<Integer> yearList = new ArrayList<Integer>();
        yearList.add(2014);
        yearList.add(2015);
        yearList.add(2016);
        yearList.add(2017);
        yearList.add(2018);
        yearList.add(2019);

        orcaReport.put(OrcaReportParam.NULL_STRING, "...");
        orcaReport.put("peopleList", peopleList);
        orcaReport.put("differentPeopleList", differentPeopleList);
        orcaReport.put("yearList", yearList);
        orcaReport.put("hello", " Hello World!");

        byte[] output = orcaReport.render();
        writeTestResults(output, ".docx");

    }

    private List<People> generatePeopleList(Integer peopleCountModifier, Integer ageModifier) {
        List<People> peopleList = new ArrayList<People>();
        for (int i = 1; i < new Random().nextInt(3) + peopleCountModifier; i++) {
            People people = new People("Person " + i, new Random().nextInt(5) + ageModifier);
            Map<Integer, Integer> puanMap = new HashMap<Integer, Integer>(0);
            puanMap.put(2014, new Random().nextInt(100));
            puanMap.put(2015, new Random().nextInt(100));
            puanMap.put(2016, new Random().nextInt(100));
            puanMap.put(2017, new Random().nextInt(100));
            // Lets skip this year
            // puanMap.put(2018, new Random().nextInt(100));
            puanMap.put(2019, new Random().nextInt(100));

            people.setPuans(puanMap);
            peopleList.add(people);
        }
        return peopleList;
    }

    public class People {

        private String name;
        private Integer age;
        private Map<Integer, Integer> puans = new HashMap<Integer, Integer>(0);

        public People(String name, Integer age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        public Map<Integer, Integer> getPuans() {
            return puans;
        }

        public void setPuans(Map<Integer, Integer> puans) {
            this.puans = puans;
        }
    }

}
