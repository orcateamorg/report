package org.orcateam.report;

import org.junit.Test;
import org.orcateam.report.engine.docx.DocXReport;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class TableWithMergedCellsTest extends BaseOrcaTest {

    @Test
    public void test() throws Exception {

        InputStream inputStream = getClass().getResourceAsStream("/TableWithMergedCells.docx");
        OrcaReport orcaReport = new DocXReport(inputStream);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy EEEE");
        SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm");

        orcaReport.put(OrcaReportParam.NULL_STRING, "");
        orcaReport.put("formId", new Random().nextInt(350));
        orcaReport.put("companyName", "Big Dollars Incoming Corp.");
        orcaReport.put("departmentName", "Most Feared Department");
        orcaReport.put("formName", "Firing Employee Form");
        orcaReport.put("formDate", dateFormat.format(new Date()));
        orcaReport.put("formHour", hourFormat.format(new Date()));
        orcaReport.put("toWhom", "the President of the Orca Republic");
        orcaReport.put("ownerName", "Murat SAYILGAN");
        orcaReport.put("location", "unknown");
        orcaReport.put("things", getTheThings());
        Employee employee = new Employee("Senior Developer", "Kamil ÖRS");
        orcaReport.put("employee", employee);

        byte[] output = orcaReport.render();
        writeTestResults(output, ".docx");

    }

    private List<Thing> getTheThings() {
        List<Thing> things = new ArrayList<Thing>(0);
        things.add(new Thing(1, "apple"));
        things.add(new Thing(4, "orange"));
        things.add(new Thing(3, "banana"));
        things.add(new Thing(2, "kiwi"));
        return things;
    }

    public class Thing {
        private Integer quantity;
        private String name;

        public Thing(Integer quantity, String name) {
            this.quantity = quantity;
            this.name = name;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public class Employee {
        private String title;
        private String name;

        public Employee(String title, String name) {
            this.title = title;
            this.name = name;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

}
