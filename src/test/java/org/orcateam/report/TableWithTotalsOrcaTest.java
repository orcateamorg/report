package org.orcateam.report;

import org.junit.Test;
import org.orcateam.report.engine.docx.DocXReport;

import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class TableWithTotalsOrcaTest extends BaseOrcaTest {

    @Test
    public void test() throws Exception {

        InputStream inputStream = getClass().getResourceAsStream("/TableWithTotals.docx");
        OrcaReport orcaReport = new DocXReport(inputStream);

        List<Expense> expenseList = expenseListGenerator(new Random().nextInt(5) + 5);

        orcaReport.put("MyUtil", new MyUtil());
        orcaReport.put("cityName", "Istanbul");
        orcaReport.put("value1", "val1");
        orcaReport.put("value2", "val2");
        orcaReport.put("expenseList", expenseList);

        byte[] output = orcaReport.render();
        writeTestResults(output, ".docx");
    }

    private List<Expense> expenseListGenerator(Integer numberOfExpenses) {
        List<Expense> expenseList = new ArrayList<Expense>(0);
        GregorianCalendar calendar = new GregorianCalendar();
        for (int i = 0; i < numberOfExpenses; i++) {
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            Date date = new Date(calendar.getTimeInMillis());
            Double meal = new Random().nextDouble() % 90 * 100 + 20;
            Double hotel = new Random().nextDouble() % 90 * 100 + 20;
            Double transport = new Random().nextDouble() % 90 * 100 + 20;
            Expense expense = new Expense(date, meal, hotel, transport);
            expenseList.add(expense);
        }
        return expenseList;
    }

    public static class MyUtil {
        public static String formatDate(Date date) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy EEE HH:mm:ss");
            return sdf.format(date);
        }

        public static String formatDouble(Double num) {
            DecimalFormat df = new DecimalFormat("#.00");
            return df.format(num);
        }
    }

    public class Expense {
        private Date date;
        private Double meal;
        private Double hotel;
        private Double transport;

        public Expense(Date date, Double meal, Double hotel, Double transport) {
            this.date = date;
            this.meal = meal;
            this.hotel = hotel;
            this.transport = transport;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public Double getMeal() {
            return meal;
        }

        public void setMeal(Double meal) {
            this.meal = meal;
        }

        public Double getHotel() {
            return hotel;
        }

        public void setHotel(Double hotel) {
            this.hotel = hotel;
        }

        public Double getTransport() {
            return transport;
        }

        public void setTransport(Double transport) {
            this.transport = transport;
        }
    }

}
